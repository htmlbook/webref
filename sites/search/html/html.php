<?php

$html = array(
  '!--','!doctype',
  'a','abbr','acronym','address','applet','area','article','aside','audio',
  'b','base','basefont','bdi','bdo','bgsound','big','blink','blockquote','body','br','button',
  'canvas', 'caption','center','cite','code','col','colgroup', 'command','comment',
  'datalist','dd','del','details','dfn','dir','div','dl','dt',
  'em','embed',
  'fieldset','figcaption','figure','font','footer','form','frame','frameset',
  'h1','h2','h3','h4','h5','h6','head','header','hgroup','hr','html',
  'i','iframe','img','input','ins','isindex',
  'kbd','keygen',
  'label','legend','li','link','listing',
  'main','map','mark','marquee','menu','meta','meter','multicol',
  'nav','nobr','noembed','noframes','noscript',
  'object','ol','optgroup','option','output',
  'p','param','plaintext','pre','progress',
  'q',
  'rp','rt','ruby',
  's','samp','script','section','select','small','span','spacer','source','strike','strong','style','sub','summary','sup',
  'table','tbody','td','textarea','tfoot','th','thead','time','title','tr','track','tt',
  'u','ul','var','wbr','video',
  'xmp',
);

?>
