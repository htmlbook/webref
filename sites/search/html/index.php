<?php

$url = 'http://' . $_SERVER['SERVER_NAME']; // Адрес сайта

require 'html.php';

$query = $_GET['q'];
$query = strtolower($query);

// Начинаем поиск только если есть строка поиска
if (isset($query)) {

  if (in_array($query, $html)) {
    $url .= '/html/' . $query;
  }
  else {
    $url .= '/search/?s=' . $query . '&cx=009874078104392781541:k8z3m71dutq&cof=FORID:9&ie=UTF-8';
  }

  header('Location: '. $url);
}

// Строка поиска пустая или нет введенного запроса
else if (empty($query) || !isset($query)) {	
  header('Location: '. $url);
	exit;
} 

?>