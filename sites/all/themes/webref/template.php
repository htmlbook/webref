<?php

function webref_preprocess_page(&$vars, $hook) {
	$status = drupal_get_http_header("status"); 
	if ($status == "404 Not Found") {  
		$vars['theme_hook_suggestions'][] = 'page__404';
		return;
	}
	else if ($status == "403 Forbidden") {  
		$vars['theme_hook_suggestions'][] = 'page__403';
		return;
	}
	
	if (arg(0) == 'taxonomy' && (arg(2) == 24 || arg(2) == 1 || arg(2) == 4)) {
		$vars['theme_hook_suggestions'][] = 'page__category';
	}
}

?>