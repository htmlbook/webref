	<div id="<?php print $block->module . '-' . $block->delta; ?>" class="block">
		<?php if($block->subject): ?><h2 class="block_title"><?php print $block->subject ?></h2><?php endif; ?>
		<div class="block_content">
			<?php print $content ?>
		</div>
	</div>
