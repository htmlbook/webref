<?php 
	$url = url($_GET['q']); 
	$t = explode("/", $url);
	$path = $t[1];
?>
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title><?= $head_title; ?></title>
		<link rel="stylesheet" href="/sites/all/themes/webref/css/foundation.min.css">
		<link rel="stylesheet" href="/sites/all/themes/webref/css/style.css">
		<script src="/sites/all/themes/webref/js/vendor/modernizr.js"></script>
		</head>
	<body>
		
		<?= $page; ?>
		
		<script src="/sites/all/themes/webref/js/vendor/jquery.js"></script>
		<script src="/sites/all/themes/webref/js/foundation.min.js"></script>
		<script src="/sites/all/themes/webref/js/foundation/foundation.topbar.js"></script>
		<script src="/sites/all/themes/webref/js/rainbow.min.js"></script>
		<?php if ($path == 'html' || $path == 'css' ): ?><script src="/sites/all/themes/webref/js/jquery.scrollTo.min.js"></script>
		<script src="/sites/all/themes/webref/js/autocomplete.js"></script>
		<script src="/sites/all/themes/webref/js/foundation/foundation.tab.js"></script>
		<? endif; ?>
		<?php if ($path == 'html'): ?>
		<script>
			if ($('#menu-menu-html').length) {
				$('#menu-menu-html .block_content').scrollTo('a.active', 500);
			}
		</script>
		<?php endif; ?>
		<?php if ($path == 'css'): ?>
		<script>
			if ($('#menu-menu-css').length) {
				$('#menu-menu-css .block_content').scrollTo('a.active', 500);
			}
		</script>
		<?php endif; ?>
		<script>$(document).foundation();</script>
	</body>
</html>