		<?php include 'header.tpl.php'; ?>
		
		<?php if ($page): ?>
		<div class="title">
			<div class="row">
				<div class="small-12 columns"><h1><?php print $title ?></h1></div>
			</div>
		</div>
		<?php endif; ?>
		
		<?php if ($tabs): ?>
		<div class="row">
			<div class="small-12 columns">
				<div class="tabs"><?php print render($tabs); ?></div>
			</div>
		</div>
		<?php endif; ?>
		
		<div class="row">
			<div class="small-12 columns">
				<?php print render($page['content']); ?>
			</div>
		</div>
		
		<?php include 'footer.tpl.php'; ?>
