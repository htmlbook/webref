<div class="<?php print $classes; ?><?php if($comment->pid) echo ' reply'; ?>">
	<div class="submitted">
		<p class="commenter-name"><?php print $author; ?></p>
		<p class="comment-time"><?php print $created; ?></p>
	</div>

	<div class="comment-text">
		<?php if ($new): ?><span class="new"><?php print $new; ?></span><?php endif; ?>
		<div class="content">
		  <?php
			hide($content['links']);
			print render($content);
		  ?>
		</div>
		<?php print render($content['links']); ?>
	</div>
</div>
