<?php 
	$url = url($_GET['q']); 
	$t = explode("/", $url);
	$path = $t[1];
?>
		<?php include 'header.tpl.php'; ?>
		
		<?php if ($page): ?>
		<div class="title">
			<div class="row">
				<div class="small-12 columns"><h1><?php print $title ?></h1></div>
				<div class="small-12 large-6 columns">
					<?php //print render($page['crumbs']); ?>
				</div>
			</div>
		</div>
		<?php endif; ?>
		
		<?php if ($tabs): ?>
		<div class="row">
			<div class="small-12 columns">
				<div class="tabs"><?php print render($tabs); ?></div>
			</div>
		</div>
		<?php endif; ?>
		
		<div class="row">
			<div class="small-8 columns main-section">
				<?php print render($page['content']); ?>
			</div>
			<aside class="small-3 small-offset-1 columns right-off-canvas-menu">
			<?php if ($path == 'html'): ?>
				<form action="/sites/search/html/" id="filter">
					<input type="search" name="q" id="ac-html" placeholder="Тег HTML" autocomplete="off">
				</form>
			<?php endif; ?>
				<?php print render($page['sidebar']); ?>
			</aside>
		</div>
		
		<?php include 'footer.tpl.php'; ?>