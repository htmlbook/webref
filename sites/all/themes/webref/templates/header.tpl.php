	<div class="off-canvas-wrap" data-offcanvas>
	<div class="inner-wrap">
		<nav class="breadcrumbs" aria-label="breadcrumbs">
			<div class="row">
				<div class="small-12 columns">
					<?php print render($page['crumbs']); ?>
				</div>
			</div>
		</nav>
		<header class="top-bar-section">
			<div class="row tab-bar">
				<div class="small-12 large-4 columns">
					<h1 title="WebReference"><a href="/"><span class="web">Web</span><span class="reference">Reference</span></a></h1>
				</div>
				<form class="search" action="/search/">
					<div class="small-12 large-7 large-offset-1 columns">
						<div class="row collapse">
							<div class="small-10 columns">
								<input type="text" placeholder="Поиск по сайту" name="s" autocomplete="off">
							</div>
							<div class="small-2 columns">
								<input type="submit" class="button postfix" value="Искать">
							</div>
						</div>
					</div>
					<input type="hidden" name="cx" value="009874078104392781541:k8z3m71dutq">
					<input type="hidden" name="cof" value="FORID:9">
					<input type="hidden" name="ie" value="UTF-8">
				</form>
			</div>
			<div class="topmenu">
				<div class="row">
					<div class="small-12 columns">
						<nav class="top-bar" data-topbar role="navigation">
							<?php print render($page['menu']); ?>
						</nav>
					</div>
				</div>
			</div>
		</header>