<?php if ($has_links): ?>
	<div class="book-navigation row">
		<div class="small-12 large-4 columns page-previous">
		<?php if ($prev_url): ?><a href="<?php print $prev_url; ?>" title="<?php print t('Go to previous page'); ?>"><?php print $prev_title; ?></a><?php endif; ?>
		</div>
		<div class="small-12 large-4 columns page-up">
			 <?php if ($parent_url): ?><a href="<?php print $parent_url; ?>" title="<?php print t('Go to parent page'); ?>"></i>Оглавление</a><?php endif; ?>
		</div>
		<div class="small-12 large-4 columns page-next">
			<?php if ($next_url): ?><a href="<?php print $next_url; ?>" title="<?php print t('Go to next page'); ?>"><?php print $next_title; ?></a><?php endif; ?>
		</div>
	</div>
<?php endif; ?>