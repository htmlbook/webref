<div class="<?php print $classes; ?>">
  <?php if (!$label_hidden): ?>
	<?php if ($element['#label_display'] == 'inline'): ?>
		<div class="field-label"<?php print $title_attributes; ?>><?php print $label ?>:&nbsp;</div>
	<?php else: ?>
		<h2 class="block_title"><?php print $label ?></h2>
	<?php endif; ?>
  <?php endif; ?>
  <div class="field-items"<?php print $content_attributes; ?>>
    <?php foreach ($items as $delta => $item): ?>
      <div class="field-item <?php print $delta % 2 ? 'odd' : 'even'; ?>"<?php print $item_attributes[$delta]; ?>><?php print render($item); ?></div>
    <?php endforeach; ?>
  </div>
</div>
