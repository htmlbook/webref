<p class="comment-tips">По умолчанию комментарии скрыты, поскольку в них могут содержаться подсказки и ответы к задачам.</p>
<p id="comment-show">Показать комментарии</p>
	<div id="comments" class="<?php print $classes; ?>"<?php print $attributes; ?>>
		<div id="comment-hide">
		<?php if ($content['comments'] && $node->type != 'forum'): ?>
			<?php print render($title_prefix); ?>
			<h2 class="title"><?php print t('Comments'); ?></h2>
			<?php print render($title_suffix); ?>
		<?php endif; ?>

		<?php print render($content['comments']); ?>

		</div>
	<?php if ($content['comment_form']): ?>
		<h2 class="title comment-form"><?php print t('Add new comment'); ?></h2>
		<?php print render($content['comment_form']); ?>
	<?php endif; ?>
	</div>
